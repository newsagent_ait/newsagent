 DROP DATABASE IF EXISTS newsAgent;
CREATE DATABASE IF NOT EXISTS newsAgent;
USE newsAgent;

drop table if exists cutomer;
CREATE TABLE customer (
    cus_id INTEGER AUTO_INCREMENT,
    firstname VARCHAR(15) NOT NULL,
    lastname VARCHAR(15) NOT NULL,
    phoneNumber VARCHAR(15) NOT NULL,
    onHoliday ENUM('YES', 'NOT') NOT NULL DEFAULT 'NOT',
    eirCode VARCHAR(7),
    address_1 VARCHAR(15) NOT NULL,
    address_2 VARCHAR(15) NOT NULL,
    PRIMARY KEY (cus_id)
);
	


INSERT INTO customer VALUES ( 1000, 'kangde ','chen', '0876019648','NOT','NS7BS7','31  the glen','coosan athlone');
INSERT INTO customer VALUES ( NULL, 'DAVII ','DD', '08761119648','NOT','NS1CS7','AIT','ATHLONE');
INSERT INTO customer VALUES ( NULL, 'araom','aa', '08761119648','Yes','ye1s5s','willow','ATHLONE');
INSERT INTO customer VALUES ( NULL, 'ryan ','DD', '08761119648',default,'NS1CS7','retresaat','ATHLONE');



SELECT * FROM customer;


drop table if exists invoice;
CREATE TABLE INVOICE (
	invoice_id integer auto_increment primary key,
    issue_date DateTime default current_timestamp,
    total decimal (5,2) Not null Default 0.00,
    cus_id integer,
    foreign key (cus_id) references customer(cus_id)
    
    );
    
    INSERT INTO invoice VALUES (null, now(),20.55,1000);
    INSERT INTO invoice VALUES (null, now(),10.55,1001);
    


SELECT * FROM invoice;

select * from invoice where cus_id = 1000;


drop table if exists publication;

Create table publication (
	pub_id integer  primary key,
    title varchar(15) not null,
    price decimal(5,2) Not null default 0.00
    
);

	INSERT INTO publication VALUES (1, "independent",3.55);
    INSERT INTO publication VALUES (2, "irish time",5.55);
    INSERT INTO publication VALUES (3, "star",4.55);
    INSERT INTO publication VALUES (4, "dailt mirro",4.55);
    INSERT INTO publication VALUES (5, "magzine",6.55);
    
    
    


SELECT * FROM publication;

drop table if exists Delivery;

Create table Delivery
(
	del_id integer auto_increment primary key,
    del_date DateTime default current_timestamp,
    del_status bool default true,
     cus_id integer,
     pub_id integer,
    foreign key (cus_id) references customer(cus_id),
    foreign key (pub_id )references publication (pub_id)
);

	INSERT INTO Delivery VALUES (null, now(),true,1000,1);
    INSERT INTO Delivery VALUES (null, now(),false,1000,2);
    INSERT INTO Delivery VALUES (null, now(),false,1003,4);

	select *  from Delivery;


    SELECT * FROM publication ,customer,delivery
where  publication.pub_id =delivery.pub_id 
order by customer.cus_id;


drop table if exists  employee;

create table employee (
		emp_id integer  primary key,
        firstName varchar(15) not null,
        lastName varchar(15) not null,
        del_id integer,
		foreign key (del_id) references delivery(del_id)
		
);
		INSERT INTO employee VALUES (1,"kangde","chen",1);
        INSERT INTO employee VALUES (2,"david","chen",2);
    

       
    
		select * from employee ;
    
    
    

